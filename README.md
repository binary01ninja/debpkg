You can see my slides about creating Ubuntu and Arch packages from snippets,
Those slides only show you path, if you are really interested to make packages,
You can take a look on links below: 


# Arch

https://wiki.archlinux.org/index.php/PKGBUILD
https://wiki.archlinux.org/index.php/Patching_packages
https://wiki.archlinux.org/index.php/makepkg
https://wiki.archlinux.org/index.php/creating_packages

# Ubuntu/Debian

https://www.debian.org/doc/debian-policy/ch-controlfields.html
https://wiki.debian.org/BuildingTutorial

